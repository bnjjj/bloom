module.exports = {
  pwa: {
    name: 'Bloom',
    iconPaths: {
      favicon32: 'kernel/static/imgs/logos/bloom_64.png',
      favicon32: 'kernel/static/imgs/logos/bloom_64.png',
      favicon16: 'kernel/static/imgs/logos/bloom_32.png',
      appleTouchIcon: 'kernel/static/imgs/logos/bloom_256.png',
      maskIcon: 'kernel/static/imgs/logos/bloom_256.png',
      msTileImage: 'kernel/static/imgs/logos/bloom_256.png',
    },
  },
};
